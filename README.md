# README #


* Code for moving WP Alverno to Omniupdate
* Do not use scripts folder as reference at this time. All JS is pulling from the internet.


### Setting up Version Control

Workflow steps for developing locally with Git version control:

1.  Download [Git](http://git-scm.com/) in order to manage versions on your local computer.
2.  Download [SourceTree](http://www.sourcetreeapp.com/) for a visual interface for working with source control.
3.  Download [WAMP](http://www.wampserver.com/) (Windows) or [MAMP](http://www.mamp.info/) (Mac) to enable local development on your
     machine.
4.  From SourceTree, create a link to an existing (or new) by clicking “Clone/New.”
5.  From Bitbucket or Git, grab the HTTPS URL (e.g. [https://github.com/virtual/Sheep-Game.git](https://github.com/virtual/Sheep-Game.git)) and paste this into your Cloned Repository’s Source Path.
6.  Set your destination folder to a new created folder that lies within your local server’s (WAMP’s) Project directory, so that you can work on and run all your files in one place.
7.  If your project has existing files, you will want to “Pull” the project down before beginning work.
8.  You can now use your favorite IDE and create and edit your code as desired. Once you have changes and want to save a version, use SourceTree to stage all your changed files. Then click “Commit” which will bring you to a window to add a message about what your changes are to this version and show all files that you have staged. Be sure to click the “Push changes immediately to origin” if you want your changes to update your project on Bitbucket/GitHub, and click “Commit.”


```
#!php
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```


### Installing Sass

Another tool we’ve incorporated is using SCSS ([Sass](http://sass-lang.com/)) to manage our CSS styles. This allows modularization, use of variables, and the ability to easily include libraries into your code.

Here’s the quick and dirty run-down of how we set up our computer to compile Sass:

*   [Download & install Ruby](http://rubyinstaller.org/) (or use `ruby -v` from Git Bash to check your version)
*   Load Sass by typing:
     `gem install sass`
*   Load compass by typing:
     `gem update --system`
     `gem install compass`
*   [Download & install Node.js](https://nodejs.org/download/) (may require restart)
*   Install Grunt:
     `npm install -g grunt-cli`
*   Load Grunt libraries:
     `npm install`

This is just a quick reference and run-down of the steps to implement a workflow similar to ours. For wonderful 3.5 hour introduction, **be sure to watch [Responsive CSS with Sass and Compass](http://wwwlyndacom.proxybz.lib.montana.edu/CSS-tutorials/Responsive-CSS-Sass-Compass/140777-2.html) with [Ray Villalobos](http://wwwlyndacom.proxybz.lib.montana.edu/Ray-Villalobos/832401-1.html "Ray Villalobos")** on Lynda.com. (All MSU staff, faculty, and students have free access to Lynda.com through the [MSU Library](https://wwwlyndacom.proxybz.lib.montana.edu:3443/Login/)!)